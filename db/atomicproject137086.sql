-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2016 at 08:57 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicproject137086`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `mail`, `birthday`, `deleted_at`) VALUES
(16, 'one', 'raihan92@rocketmail.com', '2001-01-01', NULL),
(19, 'nahiyan', 'raihan.nahiyan@gmail.com', '1999-09-23', NULL),
(23, 'Abdullah', 'abdullah@yahoo.com', '2000-02-21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `stripped_description` text NOT NULL,
  `mail` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `name`, `title`, `description`, `stripped_description`, `mail`, `deleted_at`) VALUES
(10, 'argagaerg', 'book 32', '<p>xfSdfs adfgerrg ergaergaer ergaerg</p>', 'xfSdfs adfgerrg ergaergaer ergaerg', 'tyrt@ty.com', NULL),
(11, 'adgaerg', 'book 40', '\r\n<p>Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor.</p>', '', 'tur@ty.com', NULL),
(16, 'dghj', 'book 9', '<p>Quisque volutpat condimentum velit.</p>', '', 'artaet@ty.com', NULL),
(21, 'tuuik', 'book 13', '<p>Vestibulum sapien. Proin quam. Etiam ultrices.</p>', '', 'wr67@ty.com', NULL),
(24, 'dghjdg bnthj', 'book 15', '\r\n<p>Maecenas mattis. Sed convallis tristique sem.</p>', '', 'drtert@ty.com', NULL),
(25, 'dyuj yhrth', 'book 20', '<p>Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. </p>', '', 'tyrty@ty.com', NULL),
(26, 'dgmk fhjrth', 'book 21', '<p>Fusce nec tellus sed augue semper porta.</p>', '', 'dret@ty.com', NULL),
(27, 'cghjtj sfjrtj', 'book 22', '<p>Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. </p>', '', 'yufyrty@ty.com', NULL),
(28, 'ghj hjsrtthr', 'book 24', '<p>Nunc feugiat mi a tellus consequat imperdiet. Vestibulum sapien. Proin quam. Etiam ultrices.</p>', '', 'ffyrty@ty.com', NULL),
(30, 'dtmmdtym dyjrtj', 'book 26', '<p>Praesent blandit dolor. Sed non quam. In vel mi sit amet augue congue elementum.</p>', '', 'uiti@ty.com', NULL),
(31, 'dgyj vbhre5yh', 'book 27', '<p>Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet.</p>', '', 'rttyrtu@ty.com', NULL),
(32, 'dyjsrryj dyjsrtj', 'book 29', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. </p>', '', 'tyuiru@ty.com', NULL),
(33, 'dggmdj dgyjstyyj', 'book 30', '<p>Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue.</p>', '', 'yetaf@ty.com', NULL),
(34, 'drthj tyhrst6', 'Book 41', '<p>Nam nec ante. Sed lacinia, urna non tincidunt mattis, tortor neque adipiscing diam, a cursus ipsum ante quis turpis.</p>', '', 'ukyo@ty.com', NULL),
(36, 'dtyj ytj', 'gfdg', '<p>Sed lectus. Integer euismod lacus luctus magna. Quisque cursus, metus vitae pharetra auctor, sem massa mattis sem, at interdum magna augue eget diam.</p>', '', 'ukyuki@ty.com', NULL),
(37, 'rtjn yjtyj', 'gsghsgh', '<p>Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa.</p>', '', 'gkihyui@ty.com', NULL),
(38, 'dtgm tyjry6ju', 'dfsdfsdfploj', '<p>Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla.</p>', '', 'jklyui@ty.com', NULL),
(39, 'msrttj dtyj', 'ygygfyuufy', '<p>Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa.</p>', '', 'ktyu@ty.com', NULL),
(40, 'fgagg', 'dhdh', '<p>shnahrah adrgsrheh srthstjyj eryjetjwt5tju wrthtuky fyjt y56y bryh 6yw5et6 et6u bhdh j yyhjuftgg dyutyubsrt rt ry tgh7i 7fghfgt e5ytju8iuire erg tuyui</p>', '', 'hhehhaerg@thrtgy.com', NULL),
(42, 'raihan', 'amar boi', '<p>this is my book.</p>', '', 'raihan92@rocketmail.com', NULL),
(43, 'aa', 'aa', '<p>aa aa aa</p>', '', 'aa@aa.com', NULL),
(44, 'bb', 'bb', '<p>bb bb bb</p>', '', 'bb@bb.com', NULL),
(45, 'bb', 'bb', '<p>bb bb bb</p>', '', 'bb@bb.com', NULL),
(46, 'cc', 'cc', '<p>cc cc cc</p>', '', 'cc@cc.com', NULL),
(47, 'qq', 'qq', '<p>qq qq qq</p>', '', 'qq@qq.com', NULL),
(48, 'zz', 'zz', '<p>zz zz zz zz</p>', '', 'zz@zz.com', NULL),
(49, 'zz', 'zz', '<p>zz zz zz zz</p>', 'zz zz zz zz', 'zz@zz.com', NULL),
(50, 'zz', 'zz', '<p>zz zz zz zz</p>', 'zz zz zz zz', 'zz@zz.com', NULL),
(51, 'zz', 'zz', '<p>zz zz zz zz</p>', 'zz zz zz zz', 'zz@zz.com', NULL),
(52, 'eeee', 'ee', 'ee ee ee eee eeee eeee eee', 'ee ee ee eee eeee eeee eee', 'ee@ee.com', NULL),
(53, 'vv', 'vv', '<p>vv vv vv vv vv</p>', 'vv vv vv vv vv', 'vv@vv.com', NULL),
(54, 'rr', 'rr', '<p>rr rr rrr rr rrr rr</p>', 'rr rr rrr rr rrr rr', 'rr@rr.com', NULL),
(55, 'llll', 'book llll', '<p>book llll description.</p>', 'book llll description.', 'raihan.nahiyan@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `mail`, `city`, `deleted_at`) VALUES
(2, 'ahmed', 'raihan92@rocketmail.com', 'Rajshahi', NULL),
(4, 'kretos', 'kretos@yahoo.com', 'Khulna', NULL),
(5, 'Rahim', 'rahim@yahoo.com', 'Rajshahi', NULL),
(6, 'nahiyan', 'raihan.nahiyan@gmail.com', 'Chittagong', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(60) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `deleted_at`) VALUES
(2, 'aaaaa', 'sfrer@afsef.com', NULL),
(7, 'bbbbbb', 'xcfdg@sdfsfs.cfdf', NULL),
(8, 'eeeeeee', 'sfsfwefrw@sfwer', NULL),
(9, 'fffffff', 'sddff@fghrhj.nj', NULL),
(10, 'gggggg', 'xfgjfhsrhadfgfgh@gjhfgjh.chft', NULL),
(11, 'hhhhhhh', 'ghjgkfhhk@hjkdtyju.hgh', NULL),
(12, 'iiiiiii', 'vbndg@ghjghj.hhrh', NULL),
(13, 'jjjjjjj', 'ghsfhsghsy@dghjdtj.dyjudtyu', NULL),
(14, 'kkkkkkk', 'gjtjhjdgjtyu@hjdj.gty', NULL),
(15, 'cccccc', 'bfhsrthsrth@fghsr.hfgth', NULL),
(16, 'dddddd', 'hhmcghjgtj@srty.rth', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `deleted_at`) VALUES
(6, 'one', 'Male', NULL),
(7, 'raihan', 'Male', NULL),
(8, 'ahmed', 'Male', NULL),
(9, 'jane', 'Female', NULL),
(10, 'other', 'Other', NULL),
(11, 'other more', 'Male', NULL),
(12, 'some other', 'Male', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE IF NOT EXISTS `hobby` (
`id` int(11) NOT NULL,
  `hobbies` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `hobbies`, `deleted_at`) VALUES
(5, 'Playing Football,Coding', NULL),
(6, 'Gardening,Watch Movies', NULL),
(7, 'Gardening,Coding,Reading Story Book', NULL),
(8, 'Reading Story Book,Watch Movies,Riding', NULL),
(9, 'Coding,Cricket,Swimming', NULL),
(10, 'Gardening,Playing Football', NULL),
(11, 'Swimming,Reading Story Book', NULL),
(12, 'Gardening,Coding,Swimming,Riding', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE IF NOT EXISTS `organization` (
`id` int(11) NOT NULL,
  `orgname` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`id`, `orgname`, `summary`, `deleted_at`) VALUES
(2, 'zxzx', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. ', NULL),
(3, 'zxzx', 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed', NULL),
(4, 'fghhfgh', 'Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh. Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam nec ante. Sed lacinia, urna non tincidunt mattis, tort', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE IF NOT EXISTS `profilepicture` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `mail`, `images`, `deleted_at`) VALUES
(34, 'raihan', 'raihan92@rocketmail.com', '1467308477RSG.jpg', NULL),
(36, 'cat one', 'raihan92@rocketmail.com', '1469201037imafbges.jpg', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
