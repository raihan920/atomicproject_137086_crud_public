<!DOCTYPE HTML>
<html>
    <head>
        <title> Portfolio Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap-theme.min.css"/>
        <style>
            h1{
                font-weight: bold;
            }
            p{
                font-size: 25px;
                font-weight: bold;
                padding: 5px;
            }
            a{
                margin-left: 20px;
            }
            .btn-width{
                width: 150px;
            }
        </style>
    </head>
    <body>

    <div class="container">
        <h1>Projects</h1>
        <hr />
        <p>Project 1: <a href="views/SEIP137086/Birthday/index.php" type="button" class="btn btn-success btn-lg btn-width">Birthday</a></p>
        <p>Project 2: <a href="views/SEIP137086/Email/index.php" type="button" class="btn btn-success btn-lg btn-width">Email</a></p>
        <p>Project 3: <a href="views/SEIP137086/Book/index.php" type="button" class="btn btn-success btn-lg btn-width">Book</a></p>
        <p>Project 4: <a href="views/SEIP137086/City/index.php" type="button" class="btn btn-success btn-lg btn-width">City</a></p>
        <p>Project 5: <a href="views/SEIP137086/Gender/index.php" type="button" class="btn btn-success btn-lg btn-width">Gender</a></p>
        <p>Project 6: <a href="views/SEIP137086/Hobby/index.php" type="button" class="btn btn-success btn-lg btn-width">Hobby</a></p>
        <p>Project 7: <a href="views/SEIP137086/ProfilePicture/index.php" type="button" class="btn btn-success btn-lg btn-width">Profile Picture</a></p>
        <p>Project 8: <a href="views/SEIP137086/Summary/index.php" type="button" class="btn btn-success btn-lg btn-width">Summary</a></p>
    </div>
    <script type="text/javascript" src="resources/bootstrap/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="resources/bootstrap/js/jquery.min.js"></script>
    <script type="text/javascript" src="resources/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>