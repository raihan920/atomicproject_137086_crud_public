<?php
namespace App\Bitm\SEIP137086\Birthday;
use App\Bitm\SEIP137086\Utility\Utility;
use App\Bitm\SEIP137086\Message\Message;
class Birthday
{

    public $id = "";
    public $name = "";
    public $mail = "";
    public $deleted_at = "";
    public $birthday = "";
    public $conn;


    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicproject137086") or die("Database connection failed");
    }

    public function prepare($data = "")
    {        
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("birthday",$data)){
            $this->birthday=$data['birthday'];
        }
        if (array_key_exists("mail",$data)){
            $this->mail=$data['mail'];
        }

    }
    
    public function store()
    {
        //Utility::dd($this->name);
        $userBirthDate = explode("-",$_POST['birthday']);
        $userBirthDateInDbFormat = $userBirthDate['2']."-".$userBirthDate['1']."-".$userBirthDate['0'];
        $query = "INSERT INTO `atomicproject137086`.`birthday` (`name`, `birthday`,`mail`) VALUES ('".$this->name."', '".$userBirthDateInDbFormat."','".$this->mail."')";

        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message(
                "<div class='alert alert-success'>
                    <strong>Success!</strong> Data has been stored successfully.
                </div>"
            );
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function index(){
        $_allBirthday = array();
        $query = "SELECT * FROM `atomicproject137086`.`birthday` WHERE `deleted_at` IS NULL ";
        $result = mysqli_query($this->conn, $query);
        //Utility::dd($query);
        while($row = mysqli_fetch_object($result)){
            $_allBirthday[] = $row;
        }
        return $_allBirthday;
    }

    public function view()
    {
        $query = "SELECT * FROM `atomicproject137086`.`birthday` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    public function update()
    {
        $userBirthDate = explode("-",$_POST['birthday']);
        $userBirthDateInDbFormat = $userBirthDate['2']."-".$userBirthDate['1']."-".$userBirthDate['0'];
        $query = "UPDATE `atomicproject137086`.`birthday` SET `name` = '".$this->name."',`mail` = '".$this->mail."', `birthday`='".$userBirthDateInDbFormat."' WHERE `birthday`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-success'>
                  <strong>Success!</strong> Data has been updated  successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `atomicproject137086`.`birthday` WHERE `birthday`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-danger'>
                  <strong>Deleted!</strong> Data has been deleted successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-danger'>
                  <strong>Not Deleted!</strong> Data has not been deleted successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicproject137086`.`birthday` SET `deleted_at` =".$this->deleted_at." WHERE `birthday`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Trashed!</strong> Data has been trashed successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Not Trashed!</strong> Data has not been trashed successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function trashed()
    {
        $_allBirthday = array();
        $query = "SELECT * FROM `atomicproject137086`.`birthday` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allBirthday[] = $row;
        }
        return $_allBirthday;
    }

    public function recover()
    {
        $query = "UPDATE `atomicproject137086`.`birthday` SET `deleted_at` = NULL WHERE `birthday`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-success'>
                  <strong>Recovered!</strong> Data has been recovered successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Not Recovered!</strong> Data has not been recovered successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicproject137086`.`birthday` SET `deleted_at` = NULL WHERE `birthday`.`id` IN(".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                    <div class='alert alert-success'>
                      <strong>Recovered!</strong> Selected Data has been recovered successfully.
                    </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class='alert alert-warning'>
                      <strong>Not Recovered!</strong> Selected Data has not been recovered successfully.
                    </div>");
                Utility::redirect("index.php");
            }
        }
    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            /*Utility::dd($ids);*/
            $query = "DELETE FROM `atomicproject137086`.`birthday` WHERE `birthday`.`id`  IN(".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                    <div class='alert alert-danger'>
                      <strong>Deleted!</strong> Selected Data has been deleted successfully.
                    </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class='alert alert-warning'>
                      <strong>Not Deleted!</strong> Selected Data has not been deleted successfully.
                    </div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function count()
    {
        $query = "SELECT COUNT(*) As totalItem From `atomicproject137086`.`birthday`";
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    public function paginator($pageStartFrom=0,$Limit)
    {
        $_allBirthday = array();
        $query = "SELECT * FROM `atomicproject137086`.`birthday` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn,$query);
        while ($row = mysqli_fetch_object($result)){
            $_allBirthday[]=$row;
        }
        return $_allBirthday;
    }

    /*public function getAllName(){
        $_allBook = array();
        $query = "SELECT `name` FROM `atomicproject137086`.`birthday` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        while($row = mysqli_fetch_assoc($result)){
            $_allBook[]=$row['name'];
        }
        return $_allBook;
    }*/
}