<?php
namespace App\Bitm\SEIP137086\Book;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

class Book{
    public $id="";
    public $name = "";
    public $title="";
    public $mail = "";
    public $description="";
    public $filterByTitle = "";
    public $filterByDescription="";
    public $search = "";
    public $conn;
    public $deleted_at;

    public function prepare($data=""){
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists("name",$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists("title",$data)){
            $this->title=$data['title'];
        }
        if(array_key_exists("mail",$data)){
            $this->mail=$data['mail'];
        }
        if(array_key_exists("description",$data)){
            $this->description=$data['description'];
        }
        if(array_key_exists("filterByTitle",$data)){
            $this->filterByTitle=$data['filterByTitle'];
        }
        if(array_key_exists("filterByDescription",$data)){
            $this->filterByDescription=$data['filterByDescription'];
        }
        if(array_key_exists("search",$data)){
            $this->search=$data['search'];
        }

        return $this;
    }

    public function __construct()//magic function
    {
        $this->conn=mysqli_connect("localhost","root","","atomicproject137086") or die("Database connection failed");
    }

    public function store(){
        $strippedDescription = strip_tags($this->description);
        $query="INSERT INTO `atomicproject137086`.`book` (`name`,`title`,`mail`,`description`,`stripped_description`) VALUES ('".$this->name."','".$this->title."','".$this->mail."','".$this->description."','".$strippedDescription."')";
        $result = mysqli_query($this->conn,$query);
        //Utility::dd($query);
        //$this->strippedTagDescriptionStore();
        if($result){
            Message::message(
               "<div class='alert alert-success'>
                    <strong>Success!</strong> Data has been stored successfully.
                </div>"
            );
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }

    public function index(){
        $whereClause = "1=1";
        if(!empty($this->filterByTitle)){
            $whereClause.=" AND title LIKE '%".$this->filterByTitle."%'";
        }
        if(!empty($this->filterByDescription)){
            $whereClause.=" AND description LIKE '%".$this->filterByDescription."%'";
        }
        if(!empty($this->search)){
            $whereClause.=" AND title LIKE '%".$this->search."%' OR description LIKE '%".$this->search."%'";
        }

        $_allBook = array();
        $query = "SELECT * FROM `atomicproject137086`.`book` WHERE `deleted_at` IS NULL AND ".$whereClause;
        $result = mysqli_query($this->conn, $query);
        while($row = mysqli_fetch_object($result)){
            $_allBook[] = $row;
        }
        return $_allBook;
    }

    public function view()
    {
        $query = "SELECT * FROM `atomicproject137086`.`book` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    public function update()
    {
        $strippedDescription = strip_tags($this->description);
        $query = "UPDATE `atomicproject137086`.`book` SET `title` = '".$this->title . "',`name`='".$this->name."',`mail`='".$this->mail."',`description`='".$this->description."', `stripped_description`='".$strippedDescription."' WHERE `book`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-success'>
                  <strong>Success!</strong> Data has been updated  successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `atomicproject137086`.`book` WHERE `book`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-danger'>
                  <strong>Deleted!</strong> Data has been deleted successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-danger'>
                  <strong>Not Deleted!</strong> Data has not been deleted successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicproject137086`.`book` SET `deleted_at` =".$this->deleted_at." WHERE `book`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Trashed!</strong> Data has been trashed successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Not Trashed!</strong> Data has not been trashed successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function trashed()
    {
        $_allBook = array();
        $query = "SELECT * FROM `atomicproject137086`.`book` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allBook[] = $row;
        }
        return $_allBook;
    }

    public function recover()
    {
        $query = "UPDATE `atomicproject137086`.`book` SET `deleted_at` = NULL WHERE `book`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-success'>
                  <strong>Recovered!</strong> Data has been recovered successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Not Recovered!</strong> Data has not been recovered successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicproject137086`.`book` SET `deleted_at` = NULL WHERE `book`.`id` IN(".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                    <div class='alert alert-success'>
                      <strong>Recovered!</strong> Selected Data has been recovered successfully.
                    </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class='alert alert-warning'>
                      <strong>Not Recovered!</strong> Selected Data has not been recovered successfully.
                    </div>");
                Utility::redirect("index.php");
            }
        }
    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            /*Utility::dd($ids);*/
            $query = "DELETE FROM `atomicproject137086`.`book` WHERE `book`.`id`  IN(".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                    <div class='alert alert-danger'>
                      <strong>Deleted!</strong> Selected Data has been deleted successfully.
                    </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class='alert alert-warning'>
                      <strong>Not Deleted!</strong> Selected Data has not been deleted successfully.
                    </div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function count()
    {
        $query = "SELECT COUNT(*) As totalItem From `atomicproject137086`.`book`";
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    public function paginator($pageStartFrom=0,$Limit)
    {
        $_allBook = array();
        $query = "SELECT * FROM `atomicproject137086`.`book` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn,$query);
        while ($row = mysqli_fetch_object($result)){
            $_allBook[]=$row;
        }
        return $_allBook;
    }

    public function getAllTitle(){
        $_allBook = array();
        $query = "SELECT `title` FROM `atomicproject137086`.`book` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        while($row = mysqli_fetch_assoc($result)){
            $_allBook[]=$row['title'];
        }
        return $_allBook;
    }

    ///////////////////////////////////////////
    public function getAllDescription(){
        $allBook = array();
        $query = "SELECT `stripped_description` FROM `atomicproject137086`.`book` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        while ($row = mysqli_fetch_assoc($result)){
            $allBook[] = $row['stripped_description'];
        }
        return $allBook;
    }
    ///////////////////////////////////////////
}
