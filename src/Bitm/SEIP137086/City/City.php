<?php
namespace App\Bitm\SEIP137086\City;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

class City{
    public $id="";
    public $name="";
    public $city = "";
    public $mail = "";
    public $filterByName="";
    public $filterByCity="";
    public $search="";
    public $conn;
    public $deleted_at;

    public function prepare($data=""){
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists("name",$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists("city",$data)){
            $this->city=$data['city'];
        }
        if(array_key_exists("mail",$data)){
            $this->mail=$data['mail'];
        }
        if(array_key_exists("filterByName",$data)){
            $this->filterByName=$data['filterByName'];
        }
        if(array_key_exists("filterByCity",$data)){
            $this->filterByCity=$data['filterByCity'];
        }
        if(array_key_exists("search",$data)){
            $this->search=$data['search'];
        }

        return $this;
        //Utility::d($singleItem);
    }

    public function __construct()//magic function
    {
        $this->conn=mysqli_connect("localhost","root","","atomicproject137086") or die("Database connection failed");
    }

    public function store(){

        $query="INSERT INTO `atomicproject137086`.`city` (`name`, `city`, `mail`) VALUES ('".$this->name."','".$this->city."','".$this->mail."')";
        //Utility::dd($query);
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message(
                "<div class='alert alert-success'>
                    <strong>Success!</strong> Data has been stored successfully.
                </div>"
            );
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function index(){
        $whereClause = "1=1";
        if(!empty($this->filterByName)){
            $whereClause.=" AND name LIKE '%".$this->filterByName."%'";
        }
        if(!empty($this->filterByCity)){
            $whereClause.=" AND city LIKE '%".$this->filterByCity."%'";
        }
        if(!empty($this->search)){
            $whereClause.=" AND name LIKE '%".$this->search."%' OR city LIKE '%".$this->search."%'";
        }

        $_allCity = array();
        $query = "SELECT * FROM `atomicproject137086`.`city` WHERE `deleted_at` IS NULL AND ".$whereClause;
        $result = mysqli_query($this->conn,$query);
        while($row = mysqli_fetch_object($result)){
            $_allCity[] = $row;
        }
        return $_allCity;
    }

    public function view()
    {
        $query = "SELECT * FROM `atomicproject137086`.`city` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `atomicproject137086`.`city` SET `name` = '".$this->name ."', `city` = '".$this->city."', `mail` = '".$this->mail."' WHERE `city`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-success'>
                  <strong>Success!</strong> Data has been updated  successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `atomicproject137086`.`city` WHERE `city`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-danger'>
                  <strong>Deleted!</strong> Data has been deleted successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-danger'>
                  <strong>Not Deleted!</strong> Data has not been deleted successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicproject137086`.`city` SET `deleted_at` =".$this->deleted_at." WHERE `city`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Trashed!</strong> Data has been trashed successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Not Trashed!</strong> Data has not been trashed successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function trashed()
    {
        $_allCity = array();
        $query = "SELECT * FROM `atomicproject137086`.`city` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allCity[] = $row;
        }
        return $_allCity;
    }

    public function recover()
    {
        $query = "UPDATE `atomicproject137086`.`city` SET `deleted_at` = NULL WHERE `city`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-success'>
                  <strong>Recovered!</strong> Data has been recovered successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Not Recovered!</strong> Data has not been recovered successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicproject137086`.`city` SET `deleted_at` = NULL WHERE `city`.`id` IN(".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                    <div class='alert alert-success'>
                      <strong>Recovered!</strong> Selected Data has been recovered successfully.
                    </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class='alert alert-warning'>
                      <strong>Not Recovered!</strong> Selected Data has not been recovered successfully.
                    </div>");
                Utility::redirect("index.php");
            }
        }
    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            /*Utility::dd($ids);*/
            $query = "DELETE FROM `atomicproject137086`.`city` WHERE `city`.`id`  IN(".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                    <div class='alert alert-danger'>
                      <strong>Deleted!</strong> Selected Data has been deleted successfully.
                    </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class='alert alert-warning'>
                      <strong>Not Deleted!</strong> Selected Data has not been deleted successfully.
                    </div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function count()
    {
        $query = "SELECT COUNT(*) As totalItem From `atomicproject137086`.`city`";
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    public function paginator($pageStartFrom=0,$Limit)
    {
        $_allCity = array();
        $query = "SELECT * FROM `atomicproject137086`.`city` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn,$query);
        while ($row = mysqli_fetch_object($result)){
            $_allCity[]=$row;
        }
        return $_allCity;
    }

    public function getAllCity(){
        $_allCity = array();
        $query = "SELECT `city` FROM `atomicproject137086`.`city` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        while($row = mysqli_fetch_assoc($result)){
            $_allCity[]=$row['city'];
        }
        return $_allCity;
    }

    public function getAllName(){
        $_allName = array();
        $query = "SELECT `name` FROM `atomicproject137086`.`city` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        while($row = mysqli_fetch_assoc($result)){
            $_allName[]=$row['name'];
        }
        return $_allName;
    }
}

