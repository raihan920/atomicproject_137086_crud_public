<?php
namespace App\Bitm\SEIP137086\Gender;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

class Gender{
    public $id="";
    public $name="";
    public $gender = "";
    public $conn;
    public $deleted_at;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicproject137086") or die("Database connection failed");
    }

    public function prepare($data=""){
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists("name",$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists("gender",$data)){
            $this->gender=$data['gender'];
        }
    }

    public function store(){
        $query="INSERT INTO `atomicproject137086`.`gender` (`name`, `gender`) VALUES ('".$this->name."','".$this->gender."')";
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message(
                "<div class='alert alert-success'>
                    <strong>Success!</strong> Data has been stored successfully.
                </div>"
            );
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function view()
    {
        $query = "SELECT * FROM `atomicproject137086`.`gender` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `atomicproject137086`.`gender` SET `name` = '".$this->name ."', `gender` = '".$this->gender."' WHERE `gender`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-success'>
                  <strong>Success!</strong> Data has been updated  successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `atomicproject137086`.`gender` WHERE `gender`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-danger'>
                  <strong>Deleted!</strong> Data has been deleted successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-danger'>
                  <strong>Not Deleted!</strong> Data has not been deleted successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicproject137086`.`gender` SET `deleted_at` =".$this->deleted_at." WHERE `gender`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Trashed!</strong> Data has been trashed successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Not Trashed!</strong> Data has not been trashed successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function trashed()
    {
        $_allInfo = array();
        $query = "SELECT * FROM `atomicproject137086`.`gender` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allInfo[] = $row;
        }
        return $_allInfo;
    }

    public function recover()
    {
        $query = "UPDATE `atomicproject137086`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-success'>
                  <strong>Recovered!</strong> Data has been recovered successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Not Recovered!</strong> Data has not been recovered successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicproject137086`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` IN(".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                    <div class='alert alert-success'>
                      <strong>Recovered!</strong> Selected Data has been recovered successfully.
                    </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class='alert alert-warning'>
                      <strong>Not Recovered!</strong> Selected Data has not been recovered successfully.
                    </div>");
                Utility::redirect("index.php");
            }
        }
    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            /*Utility::dd($ids);*/
            $query = "DELETE FROM `atomicproject137086`.`gender` WHERE `gender`.`id`  IN(".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                    <div class='alert alert-danger'>
                      <strong>Deleted!</strong> Selected Data has been deleted successfully.
                    </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class='alert alert-warning'>
                      <strong>Not Deleted!</strong> Selected Data has not been deleted successfully.
                    </div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function count()
    {
        $query = "SELECT COUNT(*) As totalItem From `atomicproject137086`.`gender`";
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    public function paginator($pageStartFrom=0,$Limit)
    {
        $_allInfo = array();
        $query = "SELECT * FROM `atomicproject137086`.`gender` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn,$query);
        while ($row = mysqli_fetch_object($result)){
            $_allInfo[]=$row;
        }
        return $_allInfo;
    }
}

