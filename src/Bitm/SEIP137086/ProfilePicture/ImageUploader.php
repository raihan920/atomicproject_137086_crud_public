<?php
namespace App\Bitm\SEIP137086\ProfilePicture;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

class ImageUploader{
    public $id="";
    public $name="";
    public $image_name="";
    public $mail="";
    public $deleted_at;
    public $conn;


    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicproject137086") or die("Database connection failed");
    }

    public function prepare($data = "")
    {
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("image", $data)) {
            $this->image_name = $data['image'];
        }
        if (array_key_exists("mail", $data)) {
            $this->mail = $data['mail'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        return  $this;
    }

    public function store(){
        $query="INSERT INTO `atomicproject137086`.`profilepicture` (`name`, `images`, `mail`) VALUES ('".$this->name."', '".$this->image_name."','".$this->mail."')";

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-success'>
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function index(){
        $_allinfo=array();
        $query="SELECT * FROM `atomicproject137086`.`profilepicture` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allinfo[] = $row;
        }
        return $_allinfo;
    }

    public function view(){
        $query="SELECT * FROM `atomicproject137086`.`profilepicture` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        //Utility::dd($row);//
        return $row;


    }

    public function update(){
        if(!empty($this->image_name)) {
            $query = "UPDATE `atomicproject137086`.`profilepicture` SET `name` = '" . $this->name . "', `images` = '" . $this->image_name ."', `mail` = '".$this->mail."' WHERE `profilepicture`.`id` =" . $this->id;
        }
        else {
            $query = "UPDATE `atomicproject137086`.`profilepicture` SET `name` = '" . $this->name ."' WHERE `profilepicture`.`id` =" . $this->id;
        }

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-success'>
                  <strong>Success!</strong> Data has been updated  successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Not Updated!</strong> Data has not been Updated successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function delete(){
        $query="DELETE FROM `atomicproject137086`.`profilepicture`WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-danger'>
                  <strong>Deleted!</strong> Data has been deleted successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Deleted!</strong> Data has not been deleted successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicproject137086`.`profilepicture` SET `deleted_at` =".$this->deleted_at." WHERE `profilepicture`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Trashed!</strong> Data has been trashed successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Not Trashed!</strong> Data has not been trashed successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function trashed()
    {
        $_allImage = array();
        $query = "SELECT * FROM `atomicproject137086`.`profilepicture` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allImage[] = $row;
        }
        return $_allImage;
    }
    
    public function recover()
    {
        $query = "UPDATE `atomicproject137086`.`profilepicture` SET `deleted_at` = NULL WHERE `profilepicture`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class='alert alert-success'>
                  <strong>Recovered!</strong> Data has been recovered successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class='alert alert-warning'>
                  <strong>Not Recovered!</strong> Data has not been recovered successfully.
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicproject137086`.`profilepicture` SET `deleted_at` = NULL WHERE `profilepicture`.`id` IN(".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                    <div class='alert alert-success'>
                      <strong>Recovered!</strong> Selected Data has been recovered successfully.
                    </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class='alert alert-warning'>
                      <strong>Not Recovered!</strong> Selected Data has not been recovered successfully.
                    </div>");
                Utility::redirect("index.php");
            }
        }
    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            /*Utility::dd($ids);*/
            $query = "DELETE FROM `atomicproject137086`.`profilepicture` WHERE `profilepicture`.`id`  IN(".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                    <div class='alert alert-danger'>
                      <strong>Deleted!</strong> Selected Data has been deleted successfully.
                    </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class='alert alert-warning'>
                      <strong>Not Deleted!</strong> Selected Data has not been deleted successfully.
                    </div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function setID($ids="")
    {
        $this->id = $ids;
    }

    public function count()
    {
        $query = "SELECT COUNT(*) As totalItem From `atomicproject137086`.`profilepicture`";
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    public function paginator($pageStartFrom=0,$Limit)
    {
        $_allImage = array();
        $query = "SELECT * FROM `atomicproject137086`.`profilepicture` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn,$query);
        while ($row = mysqli_fetch_object($result)){
            $_allImage[]=$row;
        }
        return $_allImage;
    }
}
?>



