<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Birthday\Birthday;
use App\Bitm\SEIP137086\Utility\Utility;
use App\Bitm\SEIP137086\Message\Message;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>

<body>
<div class="container">
    <h2>Create New Birthday</h2>
    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Enter User Name:</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter User Name">
            <label>Enter Email:</label>
            <input type="email" name="mail" class="form-control" id="mail" placeholder="Enter Email Address">
            <label>Enter User's Date of Birth:</label>
            <input type="text" name="birthday" class="form-control" id="birthday" placeholder="DD-MM-YYYY">
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
