<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Birthday\Birthday;
use App\Bitm\SEIP137086\Utility\Utility;
use App\Bitm\SEIP137086\Message\Message;

$birthday = new Birthday();
$birthday -> prepare($_GET);
$singleItem = $birthday->view();
$birthdayExploded = explode('-',$singleItem['birthday']);
$birthdayImploded = $birthdayExploded['2']."-".$birthdayExploded['1']."-".$birthdayExploded['0'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Editing Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>
<body>

<div class="container">
    <h2>Edit Birthday</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <input type="hidden" name="id" id="id" value="<?php echo $singleItem['id']?>">
            <label>Edit Birthday:</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" value="<?php echo $singleItem['name']?>">
            <label>Enter Email:</label>
            <input type="email" name="mail" class="form-control" id="mail" placeholder="Enter New Email" value="<?php echo $singleItem['mail']?>">
            <label>Enter Date of Birth:</label>
            <input type="text" name="birthday" class="form-control" id="birthday" placeholder="Enter New Date" value="<?php echo $birthdayImploded?>">

        </div>

        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>


