<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Birthday\Birthday;
use App\Bitm\SEIP137086\Utility\Utility;
use App\Bitm\SEIP137086\Message\Message;

$birthday= new Birthday();
$birthday->prepare($_GET);
$singleItem=$birthday->view();
$birthdayExploded = explode('-',$singleItem['birthday']);
$birthdayImploded = $birthdayExploded['2']."-".$birthdayExploded['1']."-".$birthdayExploded['0'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>View birthday Item</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>

</head>
<body>

<div class="container">
    <h2><?php echo $singleItem['name']?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleItem['id']?></li>
        <li class="list-group-item">Birth Day: <?php echo $birthdayImploded?></li>
        <li class="list-group-item">Email: <?php echo $singleItem['mail']?></li>
    </ul>
</div>

<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>