<!DOCTYPE html>
<html lang="en">
<head>
    <title>Create Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>

    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>

</head>

<body>
<div class="container">
    <h2>Create Book Title</h2>
    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Enter Name:</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name">
            <label>Enter Book Title:</label>
            <input type="text" name="title" class="form-control" id="title" placeholder="Enter Book Title">
            <label>Email:</label>
            <input type="email" name="mail" class="form-control" id="mail" placeholder="Enter Your Email">
            <label>Description:</label>
            <textarea class="form-control" rows="5" id="mytextarea" name="description"></textarea>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: '#mytextarea'
    });
</script>
</body>
</html>
