<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Book\Book;
use App\Bitm\SEIP137086\Utility\Utility;

$book = new Book();
$book -> prepare($_GET);
$singleItem = $book->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Editing Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
</head>
<body>

<div class="container">
    <h2>Edit Book Title</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <input type="hidden" name="id" id="id" value="<?php echo $singleItem->id?>">
            <label>Edit Name:</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter New Name" value="<?php echo $singleItem->name?>">
            <label>Edit Book Title:</label>
            <input type="text" name="title" class="form-control" id="title" placeholder="Enter Book Title" value="<?php echo $singleItem->title?>">
            <label>Email:</label>
            <input type="email" name="mail" class="form-control" id="mail" placeholder="Enter Your Email" value="<?php echo $singleItem->mail?>">
            <label>Description:</label>
            <textarea class="form-control" rows="5" id="mytextarea" name="description" ><?php echo $singleItem->stripped_description?></textarea>
        </div>

        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: '#mytextarea'
    });
</script>
</body>
</html>


