<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Book\Book;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

$book = new Book();

$availableTitle = $book->getAllTitle();
$comma_separated = '"'.implode('","',$availableTitle).'"';

$availableDescription = $book->getAllDescription();
$comma_separated_description = '"'.implode('","',$availableDescription).'"';
/*$trimmed_c_s_d = trim($comma_separated_description,'\",');
Utility::dd($trimmed_c_s_d);*/

if(array_key_exists('itemPerPage',$_SESSION)){
    if(array_key_exists('itemPerPage',$_GET)){
        $_SESSION['itemPerPage']=$_GET['itemPerPage'];
    }
}else{
    $_SESSION['itemPerPage']=5;
}

$itemPerPage = $_SESSION['itemPerPage'];

////////////////
$countItem = $book->count();
$trashedItem = $book->trashed();

$totalItem = $countItem-count($trashedItem);//counting number of arrays. output is integer.
/*Utility::dd($totalItem);*/
/*$totalItem = $book->count();*/

$totalPage = ceil($totalItem/$itemPerPage);
$pagination="";

if(array_key_exists('pageNumber',$_GET)){
    $pageNumber = $_GET['pageNumber'];
}else{
    $pageNumber = 1;
}

for($i=1;$i<=$totalPage;$i++){
    $classActive = ($pageNumber == $i)?"active":"";
    $pagination.="<li class='$classActive'><a href='index.php?pageNumber=$i'>$i</a></li>";
}
    $pageStartFrom = $itemPerPage*($pageNumber-1);

if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')){
    $allBook = $book->paginator($pageStartFrom,$itemPerPage);
}
if(strtoupper($_SERVER['REQUEST_METHOD']=='POST')){
    $allBook = $book->prepare($_POST)->index();
}
if((strtoupper($_SERVER['REQUEST_METHOD']=='GET'))&&(isset($_GET['search']))){
    $allBook = $book->prepare($_GET)->index();
}
    //$allBook = $book->index();
    //Utility::dd($_GET);
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>
<body>

<div class="container">
    <h2>All Book List</h2>
    <a href="create.php" class="btn btn-primary" role="button">Create New</a>
    <a href="trashed.php" class="btn btn-primary" role="button">View Trashed List</a>
    <a href="pdf.php" class="btn btn-default" role="button"> Download As PDF</a>
    <a href="xl.php" class="btn btn-default" role="button"> Download As XLS </a>
    <a href="mail.php" class="btn btn-default" role="button"> Send As Email </a>
    <div id="message">
        <?php
            if((array_key_exists('message',$_SESSION)) && (!empty($_SESSION['message']))){
                echo Message::message();
            }
        ?>
    </div>
    <form role="form">
        <div class="form-group">
            <label for="sel1">Select how many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option>5</option>
                <option>10</option>
                <option>15</option>
                <option>20</option>
                <option>25</option>
            </select>
        </div>
        <button type="submit" class="btn btn-default">Go!</button>
    </form>

    <form action="index.php" method="post">
        <label> Filter By Title</label>
        <input type="text" name="filterByTitle" value="" id="filterByTitle"/>
        <label> Filter By Description</label>
        <input type="text" name="filterByDescription" value="" id="filterByDescription"/>
        <button type="submit" class="btn btn-default"> Submit </button>
    </form>

    <form action="index.php" method="get">
        <label>Search</label>
        <input type="text" name="search" value=""/>
        <button type="submit" class="btn btn-default"> Search </button>
    </form>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Book title</th>
                <th>Email</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                    $sl=0;
                foreach ($allBook as $book){
                    $sl++;
                ?>
                <td><?php echo $sl+$pageStartFrom ?></td>
                <td><?php echo $book->id ?></td>
                <td><?php echo $book->name?></td>
                <td><?php echo $book->title ?></td>
                <td><?php echo $book->mail?></td>
                <td class="col-md-3"><?php echo $book->description?></td>
                <td>
                    <a href="view.php?id=<?php echo $book->id ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $book->id ?>" class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $book->id ?>" class="btn btn-danger" role="button" id="delete"
                       Onclick="return ConfirmDelete()">Delete</a>
                    <a href="trash.php?id=<?php echo $book->id ?>" class="btn btn-info" role="button">Trash</a>
                    <a href="individualmail.php?id=<?php echo $book->id?>" class="btn btn-default">Email</a>
                </td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>

    <?php if (strtoupper($_SERVER['REQUEST_METHOD']=='GET')){?>
    <ul class="pagination">
        <?php if($pageNumber>1){ $previousPage=$pageNumber-1;?>
            <li><a href='<?php echo "index.php?pageNumber=".$previousPage?>'>Prev</a></li>
            <?php
        }else{
            ?>
            <li class='disabled'><a href="">Prev</a></li>
            <?php
        }
        ?>
        <?php echo $pagination?>

        <?php if($pageNumber>=$totalPage){?>
            <li class="disabled"><a href=''>Next</a></li>
            <?php
        }else{
            //$nextPage="";
            $nextPage=$pageNumber+1;
            ?>
            <li><a href='<?php echo "index.php?pageNumber=".$nextPage?>'>Next</a></li>
            <?php
        }
        ?>
    </ul>
    <?php }?>

</div>

<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $("#message").show().delay(3000).fadeOut(1500);
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
</script>
<script type="text/javascript">
    $( function() {
        var availableTags = [<?php echo $comma_separated?>];
        $("#filterByTitle").autocomplete({
            source:availableTags
        });
    });

</script>
<script type="text/javascript">
    $(function () {
        var availableDescription = [<?php echo $comma_separated_description?>];
        $("#filterByDescription").autocomplete({
            source:availableDescription
        });
    });
</script>

</body>
</html>