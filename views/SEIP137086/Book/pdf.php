<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Book\Book;

$book = new Book();
$allInfo = $book -> index();

$trs = "";
$sl = 0;

foreach ($allInfo as $info):
    $sl++;
    $trs.="<tr>";
        $trs.="<td>$sl</td>";
        $trs.="<td>$info->id</td>";
        $trs.="<td>$info->title</td>";
        $trs.="<td>$info->stripped_description</td>";
    $trs.="</tr>";
    endforeach;

$html=<<<EOD
<div>
    <table class="table">
        <thead>
            <tr>
                <th> # </th>
                <th> ID </th>
                <th> Book Title </th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            $trs
        </tbody>
    </table>
</div>
EOD;

require_once ('../../../vendor/mpdf/mpdf/mpdf.php');

$mpdf = new mPDF();
$mpdf->WriteHTML($html);

$mpdf->Output('Book List.pdf','D');
