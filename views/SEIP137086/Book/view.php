<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Book\Book;
use App\Bitm\SEIP137086\Utility\Utility;

$book= new Book();
$book->prepare($_GET);
$singleItem=$book->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Book Item</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>

</head>
<body>

<div class="container">
    <h2><?php echo $singleItem->title?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleItem->id?></li>
        <li class="list-group-item">Name: <?php echo $singleItem->name?></li>
        <li class="list-group-item">Book Title: <?php echo $singleItem->title?></li>
        <li class="list-group-item">Email: <?php echo $singleItem->mail?></li>
    </ul>
</div>

<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>