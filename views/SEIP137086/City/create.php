<!DOCTYPE html>
<html lang="en">
<head>
    <title>Select City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>

<body>
<div class="container">
    <h2>Fill up the form with name and city</h2>
    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Enter Name:</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter Your Name">
            <label>Enter Email:</label>
            <input type="email" name="mail" class="form-control" id="mail" placeholder="Enter Email Address">
            <label> Select City: </label>
            <select class="form-control" name="city">
                <option disabled selected>Select Your City</option>
                <option value="Dhaka">Dhaka</option>
                <option value="Chittagong">Chittagong</option>
                <option value="Rajshahi">Rajshahi</option>
                <option value="Khulna">Khulna</option>
                <option value="Barishal">Barishal</option>
                <option value="Sylhet">Sylhet</option>
                <option value="Mymensingh">Mymensingh</option>
                <option value="Rangpur">Rangpur</option>
            </select>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
