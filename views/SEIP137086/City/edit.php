<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\City\City;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

$city = new City();
$city -> prepare($_GET);
$singleItem = $city->view();
//Utility::dd($singleItem->city);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>
<body>

<div class="container">
    <h2>Edit Book Title</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <input type="hidden" name="id" id="id" value="<?php echo $singleItem->id?>">
            <label>Edit Book Title:</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter Your Name" value="<?php echo $singleItem->name?>">
            <label>Edit Email:</label>
            <input type="email" name="mail" class="form-control" id="mail" placeholder="Enter New Email" value="<?php echo $singleItem->mail?>">
            <label> Select City: </label>
            <select class="form-control" name="city" value="<?php echo $singleItem->city?>">
                <option disabled>Select Your City</option>
                <option value="Dhaka" <?php if($singleItem->city=="Dhaka"){echo "selected";}?>>Dhaka</option>
                <option value="Chittagong" <?php if($singleItem->city=="Chittagong"){echo "selected";}?>>Chittagong</option>
                <option value="Rajshahi" <?php if($singleItem->city=="Rajshahi"){echo "selected";}?>>Rajshahi</option>
                <option value="Khulna" <?php if($singleItem->city=="Khulna"){echo "selected";}?>>Khulna</option>
                <option value="Barishal" <?php if($singleItem->city=="Barishal"){echo "selected";}?>>Barishal</option>
                <option value="Sylhet" <?php if($singleItem->city=="Sylhet"){echo "selected";}?>>Sylhet</option>
                <option value="Mymensingh" <?php if($singleItem->city=="Mymensingh"){echo "selected";}?>>Mymensingh</option>
                <option value="Rangpur" <?php if($singleItem->city=="Rangpur"){echo "selected";}?>>Rangpur</option>
            </select>
        </div>
        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>


