<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\City\City;
use App\Bitm\SEIP137086\Utility\Utility;

$city = new City();
$allInfo = $city -> index();
//Utility::dd($allInfo);

$trs = "";
$sl = 0;

foreach ($allInfo as $info):
    $sl++;
    $trs.="<tr>";
    $trs.="<td>$sl</td>";
    $trs.="<td>$info->id</td>";
    $trs.="<td>$info->name</td>";
    $trs.="<td>$info->city</td>";
    $trs.="</tr>";
endforeach;

$html=<<<EOD
<div>
    <table class="table">
        <thead>
            <tr>
                <th> # </th>
                <th> ID </th>
                <th>Name</th>
                <th>City</th>
            </tr>
        </thead>
        <tbody>
            $trs
        </tbody>
    </table>
</div>
EOD;

require_once ('../../../vendor/mpdf/mpdf/mpdf.php');

$mpdf = new mPDF();
$mpdf->WriteHTML($html);

$mpdf->Output('City.pdf','D');
