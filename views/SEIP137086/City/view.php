<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\City\City;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

$city= new City();
$city->prepare($_GET);
$singleItem=$city->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Cities</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>

</head>
<body>

<div class="container">
    <h2><?php echo $singleItem->name?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleItem->id?></li>
        <li class="list-group-item">Name: <?php echo $singleItem->name?></li>
        <li class="list-group-item">City: <?php echo $singleItem->city?></li>
        <li class="list-group-item">City: <?php echo $singleItem->mail?></li>
    </ul>
</div>

<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>