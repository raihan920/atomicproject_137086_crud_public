<!DOCTYPE html>
<html lang="en">
<head>
    <title>Email Subscription</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>

<body>
<div class="container">
    <h2>Create Subscriber Email</h2>
    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Enter Name:</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name">
            <label>Enter Email:</label>
            <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email">
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
