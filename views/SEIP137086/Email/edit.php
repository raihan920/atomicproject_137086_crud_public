<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Email\Email;
use App\Bitm\SEIP137086\Utility\Utility;

$email = new Email();
$email -> prepare($_GET);
$singleItem = $email->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Editing Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>
<body>

<div class="container">
    <h2>Edit Email</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <input type="hidden" name="id" id="id" value="<?php echo $singleItem->id?>">
            <label>Edit Name:</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter New Name" value="<?php $singleItem->name?>">
            <label>Edit Email:</label>                       
            <input type="text" name="title" class="form-control" id="title" placeholder="Enter New Email" value="<?php echo $singleItem->email?>">
        </div>

        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>


