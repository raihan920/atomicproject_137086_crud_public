<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Email\Email;
use App\Bitm\SEIP137086\Utility\Utility;

$email= new Email();
$email->prepare($_GET);
$singleItem=$email->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>

</head>
<body>

<div class="container">
    <h2><?php echo $singleItem->email?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleItem->id?></li>
        <li class="list-group-item">Name: <?php echo $singleItem->name?></li>
        <li class="list-group-item">Email: <?php echo $singleItem->email?></li>
    </ul>
</div>

<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>