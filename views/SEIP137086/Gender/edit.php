<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Gender\Gender;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

$gender = new Gender();
$gender -> prepare($_GET);
$singleItem = $gender->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Info</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>
<body>

<div class="container">
    <h2>Edit Book Title</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit Book Title:</label>
            <input type="hidden" name="id" id="id" value="<?php echo $singleItem->id?>">
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter Your Name" value="<?php echo $singleItem->name?>">
            <label> Select Gender: </label>
            <label><input type="radio" name="gender" value="Male" checked> Male</label>
            <label><input type="radio" name="gender" value="Female"> Female</label>
            <label><input type="radio" name="gender" value="Other"> Other</label>
        </div>
        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>


