<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Gender\Gender;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

$gender = new Gender();
$gender->prepare($_GET);
$gender->trash();