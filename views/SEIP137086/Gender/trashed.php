<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Gender\Gender;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

$gender= new Gender();
$allGender=$gender->trashed();
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>
<body>

<div class="container">
    <h2>All Trashed List</h2>
    <form action="recovermultiple.php" method="post" id="multiple">
        <a href="index.php" class="btn btn-primary" role="button">See All List</a>
        <button type="submit"  class="btn btn-primary">Recover Selected</button>
        <button type="button"  class="btn btn-primary" id="multiple_delete" Onclick="return ConfirmDelete()">Delete Selected</button>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th><input type="checkbox" onclick="toggle(this)"></th>
                    <th>Check Item</th>
                    <th>#</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                    $sl=0;
                    foreach($allGender as $gender){
                    $sl++; ?>
                    <td><input type="checkbox" name="mark[]" value="<?php echo $gender->id?>"></td>
                    <td></td>
                    <td><?php echo $sl?></td>
                    <td><?php echo $gender-> id?></td>
                    <td><?php echo $gender->name?></td>
                    <td><?php echo $gender->gender?></td>
                    <td>
                        <a href="recover.php?id=<?php echo $gender->id ?>" class="btn btn-primary" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $gender->id?>" class="btn btn-danger" role="button" id="delete" Onclick="return ConfirmDelete()">Delete</a>
                    </td>
                </tr>
                <?php }?>
                </tbody>
            </table>
    </form>
</div>
</div>

<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>

<script>
    $('#message').show().delay(1500).fadeOut(1000);

    /*$(document).ready(function(){
     $("#delete").click(function(){
     if (!confirm("Do you want to delete")){
     return false;
     }
     });
     });*/
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

    $('#multiple_delete').on('click', function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    ////////////////////////////////////////
    function toggle(source) {
        var checkboxes = document.querySelectorAll('input[type="checkbox"]');
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i] != source)
                checkboxes[i].checked = source.checked;
        }
    }
    ////////////////////////////////////////
</script>
</body>
</html>

