<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>
<body>

<div class="container">
    <h2>Select your hobby</h2>

    <form role="form" method="post" action="store.php">
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Gardening">Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Playing Football">Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Coding" >Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Cricket">Cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Swimming">Swimming</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Reading Story Book">Reading Story Book</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Watch Movies">Watch Movies</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Riding">Riding</label>
        </div>
        <input type="submit" value="Submit" class="btn btn-default">
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>

