<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Hobby\Hobby;
use App\Bitm\SEIP137086\Utility\Utility;

$hobby = new Hobby();
$hobby -> prepare($_GET);
$singleHobby = $hobby->view();
$arrayView = explode(",",$singleHobby['hobbies']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Editing Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>
<body>

<div class="container">
    <h2>Edit Hobby</h2>
    <form role="form" method="post" action="update.php" class="form-group">
        <input type="hidden" name="id" value="<?php echo $singleHobby['id']?>" />
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Gardening" <?php if (in_array("Gardening",$arrayView)){echo "checked";}?>>Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Playing Football" <?php if (in_array("Playing Football",$arrayView)){echo "checked";}?>>Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Coding" <?php if (in_array("Coding",$arrayView)){echo "checked";}?>>Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Cricket" <?php if (in_array("Cricket",$arrayView)){echo "checked";}?>>Cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Swimming" <?php if (in_array("Swimming",$arrayView)){echo "checked";}?>>Swimming</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Reading Story Book" <?php if (in_array("Reading Story Book",$arrayView)){echo "checked";}?>>Reading Story Book</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Watch Movies" <?php if (in_array("Watch Movies",$arrayView)){echo "checked";}?>>Watch Movies</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Riding" <?php if (in_array("Riding",$arrayView)){echo "checked";}?>>Riding</label>
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>


