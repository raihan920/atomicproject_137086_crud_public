<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Hobby\Hobby;
use App\Bitm\SEIP137086\Utility\Utility;

$hobby = new Hobby();
$hobby -> prepare($_GET);
$singleHobby = $hobby->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>

</head>
<body>

<div class="container">
    <h2><?php echo $singleHobby['hobbies']?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleHobby['id']?></li>
        <li class="list-group-item">Hobbies: <?php echo $singleHobby['hobbies']?></li>
    </ul>
</div>

<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>