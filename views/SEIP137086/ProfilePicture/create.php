<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap-theme.min.css">
</head>
<body>

<div class="container">
    <h2>Create Profile</h2>
    <form role="form" action="store.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Your Name"/>
        </div>
        <div class="form-group">
            <label>Enter Email:</label>
            <input type="email" name="mail" class="form-control" id="mail" placeholder="Enter Email Address"/>
        </div>
        <div class="form-group">
            <label>Upload your profile picture:</label>
            <input type="file" name="image"/>
        </div>
        <input type="submit" value="Create" class="btn btn-success"/>
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>

