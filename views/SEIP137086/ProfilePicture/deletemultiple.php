<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\ProfilePicture\ImageUploader;
use App\Bitm\SEIP137086\Utility\Utility;
use App\Bitm\SEIP137086\Message\Message;

$profilePicture = new ImageUploader();
//Utility::dd($profilePicture);
$countItem = count($_POST['mark']);

//Utility::dd($countItem);
//Utility::dd($single_info);
for ($i=0;$i<$countItem;$i++){
    $profilePicture ->setID($_POST['mark'][$i]);
    $single_info = $profilePicture->view();
    unlink($_SERVER['DOCUMENT_ROOT'].'/BITM/atomicproject_137086_crud/resources/images/'.$single_info->images);
    /*here the directory will change after the root directory*/
}
$profilePicture -> deleteMultiple($_POST['mark']);
