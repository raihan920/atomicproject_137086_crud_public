<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\ProfilePicture\ImageUploader;

$profile_picture= new ImageUploader();
$single_info=$profile_picture->prepare($_GET)->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap-theme.min.css">
</head>
<body>

<div class="container">
    <h2>Create Profile</h2>
    <form role="form" action="update.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $single_info->id?>"/>
        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control"name="name" value="<?php echo $single_info->name?>" />
        </div>

        <div class="form-group">
            <label>Email:</label>
            <input type="email" name="mail" class="form-control" id="mail" placeholder="Enter New Email" value="<?php echo $single_info->mail?>">
        </div>
        <div class="form-group">
            <label for="pwd">Upload your profile picture:</label>
            <input type="file" name="image" class="form-control">
            <img src="../../../resources/images/<?php echo $single_info->images?>"/>
        </div>
        <input type="submit" value="Update">
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>

