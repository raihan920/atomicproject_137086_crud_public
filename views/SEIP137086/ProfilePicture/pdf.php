<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\ProfilePicture\ImageUploader;

$profile = new ImageUploader();
$allInfo = $profile -> index();

$trs = "";
$sl = 0;

foreach ($allInfo as $info):
    $sl++;
    $trs.="<tr>";
        $trs.="<td>$sl</td>";
        $trs.="<td>$info->id</td>";
        $trs.="<td>$info->name</td>";
        $trs.="<td><img height='220px' width='360px' src='../../../resources/images/$info->images' /></td>";
    $trs.="</tr>";
    endforeach;

$html=<<<EOD
<div>
    <table class="table">
        <thead>
            <tr>
                <th> # </th>
                <th> ID </th>
                <th> Name </th>
                <th> Image </th>
            </tr>
        </thead>
        <tbody>
            $trs
        </tbody>
    </table>
</div>
EOD;

require_once ('../../../vendor/mpdf/mpdf/mpdf.php');

$mpdf = new mPDF();
$mpdf->WriteHTML($html);

$mpdf->Output('project profile picture.pdf','D');
