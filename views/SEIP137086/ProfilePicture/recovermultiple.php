<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
    </head>
</html>
<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\ProfilePicture\ImageUploader;
use App\Bitm\SEIP137086\Utility\Utility;
use App\Bitm\SEIP137086\Message\Message;

$profilePicture = new ImageUploader();
if (!empty($_POST['mark'])){
    $profilePicture -> recoverSeleted($_POST['mark']);
}else{
    echo "<div class='alert alert-warning'>
               <strong> No data was selected !!!!</strong>
          </div>";
}
?>