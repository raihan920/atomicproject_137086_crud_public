<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Utility\Utility;
use App\Bitm\SEIP137086\ProfilePicture\ImageUploader;


if((isset($_FILES['image'])&& !empty($_FILES['image']['name']))){
    $image_name= time().$_FILES['image']['name'];
    $temporary_location= $_FILES['image']['tmp_name'];

    move_uploaded_file( $temporary_location,'../../../resources/images/'.$image_name);
    $_POST['image']=$image_name;

}

$profile_picture= new ImageUploader();
$profile_picture->prepare($_POST)->store();

