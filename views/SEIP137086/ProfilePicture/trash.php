<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\ProfilePicture\ImageUploader;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

$profile_picture = new ImageUploader();
$profile_picture->prepare($_GET);
$profile_picture->trash();