<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\ProfilePicture\ImageUploader;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

$profile_picture = new ImageUploader();
$profile_picture->prepare($_GET);
$singleProfile=$profile_picture->view();

//Utility::d($allinfo);
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
    <!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">-->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->

</head>
<body>

<div class="container">
    <h2>All Info List</h2>
    <a href="create.php" class="btn btn-primary" role="button">Create New</a>
    <a href="trashed.php" class="btn btn-primary" role="button">View Trashed list</a>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Image Preview</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?php echo $singleProfile-> id?></td>
                <td><?php echo $singleProfile->name?></td>
                <td><?php echo $singleProfile->mail?></td>
                <td><img src="../../../resources/images/<?php echo $singleProfile->images ?>" alt="image" height="500px" width="500px" class="img-responsive"> </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
