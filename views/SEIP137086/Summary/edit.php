<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Summary\Summary;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

$summary = new Summary();
$summary -> prepare($_GET);
$singleItem = $summary->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Summary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>
<body>

<div class="container">
    <h2>Edit Organization Summary:</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit Summary:</label>
            <input type="hidden" name="id" id="id" value="<?php echo $singleItem->id?>">
            <input type="text" name="orgname" class="form-control" id="orgname" placeholder="Enter Organization Name" value="<?php echo $singleItem->orgname?>">
            <br/>
            <label> Organization Summary: <br/>
                <textarea rows="12" cols="30" name="summary"><?php echo $singleItem->summary?></textarea>
            </label>
        </div>
        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>


