<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Summary\Summary;
use App\Bitm\SEIP137086\Message\Message;
use App\Bitm\SEIP137086\Utility\Utility;

$summary = new Summary();
if(array_key_exists('itemPerPage',$_SESSION)){
    if(array_key_exists('itemPerPage',$_GET)){
        $_SESSION['itemPerPage']=$_GET['itemPerPage'];
    }
}else{
    $_SESSION['itemPerPage']=5;
}

$itemPerPage = $_SESSION['itemPerPage'];

////////////////
$countItem = $summary->count();
$trashedItem = $summary->trashed();

$totalItem = $countItem-count($trashedItem);//counting number of arrays. output is integer.
/*Utility::dd($totalItem);*/
/*$totalItem = $summary->count();*/

$totalPage = ceil($totalItem/$itemPerPage);
$pagination="";

if(array_key_exists('pageNumber',$_GET)){
    $pageNumber = $_GET['pageNumber'];
}else{
    $pageNumber = 1;
}

for($i=1;$i<=$totalPage;$i++){
    $classActive = ($pageNumber == $i)?"active":"";
    $pagination.="<li class='$classActive'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

    $pageStartFrom = $itemPerPage*($pageNumber-1);
    $allSummary = $summary->paginator($pageStartFrom,$itemPerPage);

    //$allSummary = $summary->index();
    //Utility::dd($allSummary);
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap-theme.min.css"/>
</head>
<body>

<div class="container">
    <h2>List of All Organization Summary.</h2>
    <a href="create.php" class="btn btn-primary" role="button">Create New</a>
    <a href="trashed.php" class="btn btn-primary" role="button">View Trashed List</a>
    <div id="message">
        <?php
            if((array_key_exists('message',$_SESSION)) && (!empty($_SESSION['message']))){
                echo Message::message();
            }
        ?>
    </div>
    <form role="form">
        <div class="form-group">
            <label for="sel1">Select how many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option>5</option>
                <option>10</option>
                <option>15</option>
                <option>20</option>
                <option>25</option>
            </select>
        </div>
        <button type="submit" class="btn btn-default">Go!</button>
    </form>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Organization Name</th>
                <th>Organization Summary</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                    $sl=0;
                foreach ($allSummary as $summary){
                    $sl++;
                ?>
                <td><?php echo $sl+$pageStartFrom ?></td>
                <td><?php echo $summary->id ?></td>
                <td><?php echo $summary->orgname?></td>
                <td class="col-md-6"><?php echo $summary->summary ?></td>
                <td>
                    <a href="view.php?id=<?php echo $summary->id ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $summary->id ?>" class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $summary->id ?>" class="btn btn-danger" role="button" id="delete"
                       Onclick="return ConfirmDelete()">Delete</a>
                    <a href="trash.php?id=<?php echo $summary->id ?>" class="btn btn-info" role="button">Trash</a>
                </td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>

    <ul class="pagination">
        <?php if($pageNumber>1){ $previousPage=$pageNumber-1;?>
            <li><a href='<?php echo "index.php?pageNumber=".$previousPage?>'>Prev</a></li>
            <?php
        }else{
            ?>
            <li class='disabled'><a href="">Prev</a></li>
            <?php
        }
        ?>
        <?php echo $pagination?>

        <?php if($pageNumber>=$totalPage){?>
            <li class="disabled"><a href=''>Next</a></li>
            <?php
        }else{
            //$nextPage="";
            $nextPage=$pageNumber+1;
            ?>
            <li><a href='<?php echo "index.php?pageNumber=".$nextPage?>'>Next</a></li>
            <?php
        }
        ?>
    </ul>
</div>

<script type="text/javascript" src="../../../resources/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/bootstrap/js/bootstrap.min.js"></script>

<script>
    $('#message').show().delay(3000).fadeOut(1500);
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
</script>
</body>
</html>